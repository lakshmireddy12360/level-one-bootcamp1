#include<stdio.h>
struct fract
{
    int a; //numerator
    int b; //denominator
};
typedef struct fract fr;
void input(int n, fr f[n])
{
    for(int i = 0;i < n;i++)
    {
        printf("Enter the numerator of fraction");
        scanf("%d",&f[i].a);
        printf("Enter the denominator of fraction ");
        scanf("%d",&f[i].b);
    }
}
int gcd(int g1,int g2)
{
     int g=0;
     for(int i=1;i<=g1&&i<=g2;i++)
    {
        if(g1%i==0 && g2%i==0)
        {
            g=i;
        }
    }
      return g;
}
fr reduce(int n,fr f[n] )
{
    fr s;
    s.b = 1;
    for(int i = 0;i < n;i++)
     s.b = s.b * f[i].b;

    
    s.a = 0;
    for(int i = 0; i < n;i++)
    {
        int temp = f[i].a;
        for(int j = 0;j < n;j++)
        {
            if(i!=j)
            {
                temp = temp *f[i].b;
            }
       } 
       s.a = s.a + temp; 
}

    int g1=s.a;
    int g2=s.b;
    s.a=s.a/gcd(g1,g2);
    s.b=s.b/gcd(g1,g2);
    return s;


    }   

   
void print(int n,fr f[n],fr s)
{
     printf("Sum of ");
     for(int i = 0;i < n-1; i++)
     printf("%d/%d +",f[i].a,f[i].b);
     printf("%d/%d=%d/%d",f[n-1].a,f[n-1].b,s.a,s.b);
}
int main()
{
    int n;
    printf("Enter the number of fractions:\n");
    scanf("%d",&n);
    fr f[n];
    input(n,f);
    fr s = reduce(n,f);
    print(n,f,s);
}


